package lab01;

public class HW1 {
	public static void main(String[] args) {
		System.out.println(" Article No.1.1 --------------------------------");
		draw1(4);
		space();
		System.out.println(" Article No.1.2 --------------------------------");
		draw2(4);
		space();
		System.out.println(" Article No.1.3 --------------------------------");
		draw3(4);
		space();
		System.out.println(" Article No.1.4 --------------------------------");
		draw4(4);
		space();
		System.out.println(" Article No.1.5 --------------------------------");
		draw5(4);
		space();
		System.out.println(" Article No.1.6 --------------------------------");
		draw6(4);
		space();
		System.out.println(" Article No.1.7 --------------------------------");
		draw7(4);
		space();
		System.out.println(" Article No.1.8 --------------------------------");
		draw8(4);
		space();
	}

	public static void draw1(int n) {
		for (int j = 0; j < n; j++) { 
			System.out.print(" *"); 
		}
		System.out.println();
		System.out.println(" n: " + n); 
		}

	public static void draw2(int n) {
		for (int i = 0; i < n; i++) { 
			for (int j = 0; j < n; j++) { 
				System.out.print(" *"); 
			}
			System.out.println(); 
			}
		System.out.println(" n: " + n); 
		}

	public static void draw3(int n) {
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++) {
				System.out.print(j);
			}
			System.out.println();
		}
		System.out.println(" n: " + n);
	}

	public static void draw4(int n) {
		for (int i = 1; i <= n; i++) {
			for (int j = n; j >= 1; j--) {
				System.out.print(j);
			}
			System.out.println();
		}
		System.out.println(" n: " + n);
	}

	public static void draw5(int n) {
		for (int i = 1; i <= n; i++) {
			for (int j = 0; j < n; j++) {
				System.out.print(i);
			}
			System.out.println();
		}
		System.out.println(" n: " + n);
	}

	public static void draw6(int n) {
		for (int i = n; i >= 1; i--) {
			for (int j = 1; j <= n; j++) {
				System.out.print(i);
			}
			System.out.println();
		}
		System.out.println(" n: " + n);
	}

	public static void draw7(int n) {
		int k = 1;
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++, k++) {
				System.out.printf(" %-3d", k);
			}
			System.out.print("\n");
		}
		System.out.println(" n: " + n);
	}

	public static void draw8(int n) {
		int k = 16;
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++, k--) {
				System.out.printf(" %-3d", k);
			}
			System.out.print("\n");
		}
		System.out.println(" n: " + n);
	}

	public static void space() {
		System.out.println(" ");
	}
}
